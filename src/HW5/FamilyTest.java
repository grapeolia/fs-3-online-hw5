package HW5;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Captor;

import static HW5.HappyFamily.setUpDailySchedule;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class FamilyTest {

    private Family module;

    @BeforeEach
    public void setUp() {
        String[][] schedule = setUpDailySchedule();
        Human mother = new Human("Mother", "MotherLastName", 1979);
        Human father = new Human("Father", "FatherLastName", 1974);
        Human child = new Human("Child1", "ChildLastName", 2000);
        Human childToAdd = new Human("Child2","Child2Surname", 2019);
        Pet catBoria = new Pet(Species.CAT,"Boria",2, 46, new String[]{"eat","sleep"});
        module = new Family(mother,father,new Human[]{child},catBoria);
    }

    @Test
    public void getMother(){
        String actual = module.getMother().toString();
        String expected = "Human{name='Mother', surname='MotherLastName', year=1979, iq=0, schedule=null}";
        assertEquals(expected, actual);
    }

    @Test
    public void getFather(){
        String actual = module.getFather().getSchedule().toString();
        String expected = "Human{name='Father', surname='FatherLastName', year=1974, iq=0, schedule=[[MONDAY, Get Enough Sleep], [TUESDAY, Rise Early], [WEDNESDAY, Meditate], [THURSDAY, Workout], [FRIDAY, Eat A Good Breakfast], [SATURDAY, Take A Nap], [SUNDAY, Take Breaks To Re-energize]]}";
        assertEquals(expected, actual);
    }

    @Test
    public void getChildren(){
        String actual = module.getChildren()[0].toString();
        String expected = "Human{name='Child1', surname='ChildLastName', year=2000, iq=0, schedule=null}";
        assertEquals(expected, actual);
    }

    @Test
    public void getPet(){
        String actual = module.getPet().toString();
        String expected = "CAT{nickname='Boria', age=2, trickLevel=46, habits=[eat, sleep]}";
        assertEquals(expected, actual);
    }

    @Test
    public void deleteChildByExistingIndex(){
       Boolean actual = module.deleteChild(0);
       Boolean expected = true;
       assertEquals(expected,actual);
   }

    @Test
    public void deleteChildByWrongIndex(){
        Boolean actual = module.deleteChild(3);
        Boolean expected = false;
        assertEquals(expected,actual);
    }

    @Test
    public void deleteChildByExistingObject(){
        Boolean actual = module.deleteChild(module.getChildren()[0]);
        Boolean expected = true;
        assertEquals(expected,actual);
    }

    @Test
    public void deleteChildByWrongObject(){
        Boolean actual = module.deleteChild(null);
        Boolean expected = false;
        assertEquals(expected,actual);
    }

    @Test
    public void countFamily(){
        int actual = module.countFamily();
        int expected = 3;
        assertEquals(expected,actual);
    }


}
