package HW5;

import java.util.Arrays;

public class HappyFamily {
    public static void main(String[] args) {
        Human mother1 = new Human("Mother", "MotherLastName", 1979);
        Human father1 = new Human("Father", "FatherLastName", 1974);
        Human child1 = new Human("Child1", "ChildLastName", 2000);
        Human childToAdd = new Human("Child2","Child2Surname", 2019);
        String[][] schedule = setUpDailySchedule();
        Pet catBoria = new Pet(Species.CAT,"Boria",2, 46, new String[]{"eat","sleep"});
        Pet dogRob = new Pet(Species.DOG, "Rob",3,67, new String[]{"eat","sleep","jump"});
        Family family1 = new Family();
        father1.setSchedule(schedule);
        family1.setMother(mother1);
        family1.setFather(father1);
        family1.setChildren(new Human[]{child1});
        family1.addChild(childToAdd);
        family1.deleteChild(0);
        family1.setPet(dogRob);
        Family familyNew = new Family(mother1,father1,new Human[]{child1},catBoria);
        familyNew.deleteChild(child1);



    }

    public static String[][] setUpDailySchedule(){
        String[][] schedule = new String[7][2];
        schedule[0][0] = DayOfWeek.MONDAY.name();
        schedule[1][0] = DayOfWeek.TUESDAY.name();
        schedule[2][0] = DayOfWeek.WEDNESDAY.name();
        schedule[3][0] = DayOfWeek.THURSDAY.name();
        schedule[4][0] = DayOfWeek.FRIDAY.name();
        schedule[5][0] = DayOfWeek.SATURDAY.name();
        schedule[6][0] = DayOfWeek.SUNDAY.name();

        schedule[0][1] = "Get Enough Sleep";
        schedule[1][1] = "Rise Early";
        schedule[2][1] = "Meditate";
        schedule[3][1] = "Workout";
        schedule[4][1] = "Eat A Good Breakfast";
        schedule[5][1] = "Take A Nap";
        schedule[6][1] = "Take Breaks To Re-energize";

        return schedule;
    }

}